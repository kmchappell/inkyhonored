[![Build Status](https://gitlab.com/pages/jekyll/badges/master/pipeline.svg)](https://gitlab.com/pages/jekyll/-/pipelines?ref=master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

## A small Dwarf Fortress blog

View live site at https://kmchappell.gitlab.io/inkyhonored

Sometimes I play a little Dwarf Fortress. I thought it would be fun to tell the story of one fortress in detail through a little blog. Nothing fancy, just for entertainment!

Powered by GitLab Pages, based on the [built in Jekyll template](https://gitlab.com/pages/jekyll).